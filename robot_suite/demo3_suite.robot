*** Settings ***
Library    SeleniumLibrary


*** Test Cases ***
TC1
    Open Browser        url=https://www.facebook.com/       browser=chrome
    ${actual_title}     Get Title
    Log    ${actual_title}
    ${current_url}      Get Location
    Log    ${current_url}
    Close Browser

TC2
    Open Browser        url=https://www.facebook.com/       browser=chrome
    Input Text          id=email        waqasafsar787@gmail.com
    Input Password      id=pass         waqasafsar787@gmail.com
    Click Element       name=login
    Close Browser

TC3
    Open Browser        browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    3s
    Go To    url=https://www.db4free.net/
    Click Element       partial link=phpMyAdmin
    Switch Window       phpMyAdmin
    Input Text          id=input_username       admin
    Input Text          id=input_password       admin123
    Click Element       id=input_go
    Element Should Contain      id=pma_errors         Access denied for user
    #Element Should Contain    //div[@role='alert']

    Close Window
    Sleep    10s
    Close Browser



