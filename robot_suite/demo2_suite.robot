*** Settings ***
Library    String
Library    Collections

*** Variables ***
${Browser_Name}     chrome
@{COLOR}      red     blue     green

*** Test Cases ***
TC1
    Set Global Variable     ${num1}         45
    Log To Console    ${num1}
    Log To Console    Please launch ${Browser_Name} for ${num1}
    Log To Console    ${COLOR}
    Log To Console    ${COLOR[1]}

TC2
    Log To Console    ${Browser_Name}

TC3
    ${output}       Convert To Upper Case       ${Browser_Name}
    Log To Console    ${output}

TC4
    ${sal1}     Set Variable    $123456
    ${sal2}     Set Variable    $100000
    ${sal1}     Remove String    ${sal1}    $
    ${sal2}     Remove String    ${sal2}    $
    ${result}   Evaluate    ${sal1} + ${sal2}
    Log To Console    ${result}

TC5
    @{fruits}       Create List     mango       apple      grapes
    &{mydic}        Create Dictionary   empid=100        empname=John       salary=7777
    Append To List      ${fruits}       kiwi
    Insert Into List    ${fruits}    0    pineapple
    #Remove Values From List    ${fruits}    apple
    Log To Console    ${fruits}
    Log         ${fruits}
    Log List    ${fruits}
    Log Dictionary    ${mydic}
    Log    ${mydic}[empname]
    
TC6
    ${num}      Set Variable    2
    IF    ${num} < 0
          Log    The number is negative
    ELSE
           Log    The number is positive
    END

TC7
    @{fruits}       Create List     mango       apple      grapes   pineapple
    ${size}         Get Length    ${fruits}
    FOR    ${i}    IN RANGE     0       ${size}
        Log    ${fruits}[${i}]
    END
            
       

    

